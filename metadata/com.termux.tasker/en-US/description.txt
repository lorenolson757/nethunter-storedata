The Termux:Task add-on provides an easy way to call Termux executables from Tasker and compatible apps.

1. Create a new Tasker Action.
2. In the resulting Select Action Category dialog, select Plugin.
3. In the resulting dialog, select Termux:Task.
4. Edit the configuration to specify the executable in ~/.termux/tasker/ to execute, and if it should be executed in the background (the default) or in a new terminal session.

NOTE: This is an add-on app which requires both Termux and Tasker (or a Tasker-compatible app) to use.

.
